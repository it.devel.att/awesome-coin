const hre = require("hardhat");

async function main() {
  const AwesomeCoin = await hre.ethers.getContractFactory("AwesomeCoin");
  const initialBalance = 100_000_000;
  const awesomeCoin = await AwesomeCoin.deploy(initialBalance);

  await awesomeCoin.deployed();

  console.log("AwesomeCoin deployed to:", awesomeCoin.address);
  console.log("AwesomeCoin total coins supply:", (await awesomeCoin.totalSupply()).toNumber());
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
