const { expect } = require("chai");

describe("Token contract", () => {

  const zeroAddress = '0x0000000000000000000000000000000000000000';
  let contract;
  let owner;
  let addr1;
  let addr2;
  let addrs;
  let initialBalance = 100_000_000;
  let clean;

  before(async () => {
    const Token = await ethers.getContractFactory("AwesomeCoin");
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    contract = await Token.deploy(initialBalance);
 
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: []
    });
  });

  afterEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [clean]
    });
    clean = await network.provider.request({
      method: "evm_snapshot",
      params: []
    });
  });

  it("Should deploy with coins to owner", async () => {
    const result = await contract.balanceOf(owner.address);

    expect(result.toNumber()).to.equal(initialBalance);
  });

  it("Owner can mint to address more coins", async () => {
    const result = await contract.mint(addr1.address, 100_000);

    const totalSupplyResult = await contract.totalSupply();
    const addr1Balance = await contract.balanceOf(addr1.address);

    expect(totalSupplyResult.toNumber()).to.equal(initialBalance + 100_000);
    expect(addr1Balance.toNumber()).to.equal(100_000);
  });

  it("Cannot mint to zero address", async () => {
    await expect(contract.mint(zeroAddress, 100_000)).to.be.revertedWith('ERC20: mint to the zero address');
  });

  it("Not minter(owner) can't mint to address more coins", async () => {
    await expect(contract.connect(addr1).mint(addr1.address, 100_000)).to.be.revertedWith('Only minters can perform operation');

    const totalSupplyResult = await contract.totalSupply();
    const addr1Balance = await contract.balanceOf(addr1.address);

    expect(totalSupplyResult.toNumber()).to.equal(initialBalance);
    expect(addr1Balance.toNumber()).to.equal(0);
  });

  it("Can burn amount of coins", async () => {
    const result = await contract.burn(100_000);
    
    const totalSupplyResult = await contract.totalSupply();
    const ownerBalance = await contract.balanceOf(owner.address);

    expect(totalSupplyResult.toNumber()).to.equal(initialBalance - 100_000);
    expect(ownerBalance.toNumber()).to.equal(initialBalance - 100_000);
  });

  it("Anyone can burn amount of coins", async () => {
    await contract.transfer(addr1.address, 100_000);
    const result = await contract.connect(addr1).burn(100_000);
    
    const totalSupplyResult = await contract.totalSupply();
    const addr1Balance = await contract.balanceOf(addr1.address);

    expect(totalSupplyResult.toNumber()).to.equal(initialBalance - 100_000);
    expect(addr1Balance.toNumber()).to.equal(0);
  });

  it("Cannot burn amount of coins more than balance amount", async () => {
     await expect(contract.burn(initialBalance + 1)).to.be.revertedWith('Cannot burn more than available balance');
  });

  it("Should transfer from owner to addr1", async () => {
    await contract.transfer(addr1.address, 1000);

    const addrBalance = await contract.balanceOf(addr1.address);
    const ownerBalance = await contract.balanceOf(owner.address);


    expect(addrBalance.toNumber()).to.equal(1000);
    expect(ownerBalance.toNumber()).to.equal(initialBalance - 1000);
  });

  it("Cannot transfer to zero address", async () => {
    await expect(contract.transfer(zeroAddress, 1000)).to.be.revertedWith('ERC20: transfer to the zero address');
  });

  it("Cannot transfer more amount then balance", async () => {
    await contract.transfer(addr1.address, 1000);

    await expect(contract.connect(addr1).transfer(addr2.address, 1001)).to.be.revertedWith('ERC20: transfer amount exceeds balance')
  });

  it("Should show total supply", async () => {
    const result = await contract.totalSupply();

    expect(result.toNumber()).to.equal(initialBalance);
  });

  it("Should show name", async () => {
    const result = await contract.name();

    expect(result).to.equal("Awesome Coin");
  });

  it("Should show symbol", async () => {
    const result = await contract.symbol();

    expect(result).to.equal("AWC");
  });

  it("Should show decimals", async () => {
    const result = await contract.decimals();

    expect(result).to.equal(18);
  });

  it("Should approve and allowance check", async () => {
    await contract.approve(addr1.address, 100_000);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(100_000);
  });

  it("Cannot approve zero address", async () => {
    await expect(contract.approve(zeroAddress, 100_000)).to.be.revertedWith('ERC20: approve to the zero address');
  });

  it("Should transfer from owner to addr2 through addr1", async () => {
    await contract.approve(addr1.address, 100_000);

    const ownerBalance = await contract.balanceOf(owner.address);
    expect(ownerBalance.toNumber()).to.equal(initialBalance);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(100_000);


    await contract.connect(addr1).transferFrom(owner.address, addr2.address, 90_000);

    const addr2Balance = await contract.balanceOf(addr2.address);
    expect(addr2Balance.toNumber(), 90_000)

    const newOwnerBalance = await contract.balanceOf(owner.address);
    expect(newOwnerBalance.toNumber()).to.equal(initialBalance - 90_000);

    const newAllowance = await contract.allowance(owner.address, addr1.address);
    expect(newAllowance.toNumber()).to.equal(10_000);
  });

  it("Cannot transfer from owner to addr2 through addr1 more then approve amount", async () => {
    await contract.approve(addr1.address, 10_000);

    const ownerBalance = await contract.balanceOf(owner.address);
    expect(ownerBalance.toNumber()).to.equal(initialBalance);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(10_000);


    await expect(contract.connect(addr1).transferFrom(owner.address, addr2.address, 10_001)).to.be.revertedWith('ERC20: transfer amount exceeds allowance');
  });

  it("Cannot transferFrom from zero address", async () => {
    await expect(contract.connect(addr1).transferFrom(zeroAddress, addr2.address, 10_001)).to.be.revertedWith('ERC20: transfer from the zero address');
  });

  it("Cannot transferFrom to zero address", async () => {
    await expect(contract.connect(addr1).transferFrom(owner.address, zeroAddress, 10_001)).to.be.revertedWith('ERC20: transfer to the zero address');
  });

  it("Even if approve more than balance cannot transferFrom more then balance", async () => {
    await contract.transfer(addr1.address, 10_000);
    await contract.connect(addr1).approve(addr2.address, 10_001)

    await expect(contract.connect(addr2).transferFrom(addr1.address, owner.address, 10_001)).to.be.revertedWith('ERC20: transfer amount exceeds balance');
  });

  it("Cannot transfer from not approved address", async () => {
    await contract.approve(addr1.address, 10_000);

    const result = await contract.allowance(owner.address, addr1.address);
    expect(result.toNumber()).to.equal(10_000);


    await expect(contract.connect(addr2).transferFrom(owner.address, addr2.address, 10_001)).to.be.revertedWith('ERC20: transfer amount exceeds allowance');
  });
});
