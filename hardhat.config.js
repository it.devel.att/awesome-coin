require('dotenv').config();
require("@nomiclabs/hardhat-waffle");
require('solidity-coverage');
require('./tasks/awesome-coin-tasks');

task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// For local testing purpose. Private key from local node, feel free to change it
const localPrivateKey = process.env.LOCAL_PRIVATE_KEY || "0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e";

const rinkebyURL = process.env.RINKEBY_URL || "https://rinkeby.infura.io/v3/secret_hash";
const rinkebyPrivateKey = process.env.RINKEBY_PRIVATE_KEY || "0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e";


module.exports = {
  solidity: {
    version: "0.8.4",
    settings: {
        optimizer: {
          enabled: true,
          runs: 200
        },
    },
  },
  defaultNetwork: 'hardhat',
  networks: {
    local: {
      url: `http://127.0.0.1:8545`,
      accounts: [localPrivateKey]
    },
    rinkeby: {
      url: rinkebyURL,
      accounts: [rinkebyPrivateKey],
    }
  },

};
