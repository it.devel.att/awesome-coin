async function getContract(contractAddress) {
    const Token = await ethers.getContractFactory("AwesomeCoin");
    const contract = await Token.attach(contractAddress);
    return contract
}

task("balance", "Display balance amount")
    .addParam("contract", "The address of contract")
    .addParam("address", "The address of wallet")
    .setAction(async (taskArgs) => {
        const contract = await getContract(taskArgs.contract);

        const result = await contract.balanceOf(taskArgs.address);
        console.log(`Address ${taskArgs.address}. Amount: ${result.toNumber()}`);
    });

task("transfer", "Transfer amount to address")
    .addParam("contract", "The address of contract")
    .addParam("address", "The address to transfer")
    .addParam("amount", "Amount of coins to transfer")
    .setAction(async (taskArgs) => {
        const contract = await getContract(taskArgs.contract);

        await contract.transfer(taskArgs.address, taskArgs.amount);
        console.log(`Transfer ${taskArgs.amount} coins to ${taskArgs.address} address`);
    });

task("approve", "Approve amount to address")
    .addParam("contract", "The address of contract")
    .addParam("address", "The address of spender")
    .addParam("amount", "Amount of coins to approve")
    .setAction(async (taskArgs) => {
        const contract = await getContract(taskArgs.contract);

        await contract.approve(taskArgs.address, taskArgs.amount);
        console.log(`Approve ${taskArgs.amount} coins to ${taskArgs.address} address`);
    });

task("transferFrom", "Tranfer amount of coins from address to address")
    .addParam("contract", "The address of contract")
    .addParam("from", "From address")
    .addParam("to", "To address")
    .addParam("amount", "Amount of coins")
    .setAction(async (taskArgs) => {
        const contract = await getContract(taskArgs.contract);

        await contract.transferFrom(taskArgs.from, taskArgs.to, taskArgs.amount);
        console.log(`Transfer ${taskArgs.amount} coins from ${taskArgs.from} address to ${taskArgs.to} address`);
    });